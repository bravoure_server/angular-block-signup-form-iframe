## Angular Block Signup Form Iframe / Bravoure component

This Component creates a Block Signup form, that uses an iframe from the core project.

### **Versions:**

1.0.1    - Fix for wrong component folder path
1.0.0    - Initial version

---------------------
