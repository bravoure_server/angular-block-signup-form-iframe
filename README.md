# Bravoure - Block Signup Form Iframe

## Directive: Code to be added:

        <block-signup-form-iframe></block-signup-form-iframe>

## Use

it takes the iframe from the core and it loads it

### Instalation

in the bower.json file of the base of the application,(src/app/bower.json)

add

    {
      ...
      "dependencies": {
        ...
        "angular-block-signup-form-iframe": "1.0"
      }
    }

and the run in the terminal 
    
    // in the correct location (src/app)
    
    bower install
