(function () {
    'use strict';

    function blockSignupFormIframe (PATH_CONFIG, $rootScope){
        return {
            restrict: "E",
            replace: "true",
            link: function (scope, element, attrs) {

            },
            controller: function ($scope) {
                $scope.signup_url = host + 'signup?lang=' + PATH_CONFIG.current_language + '&redirect_uri=' + host + 'authorize';

                $scope.showIframe = ($scope.$state.params.identifier == 'top.sign_up_page') ? true : false;

                if (!data.authenticationByPage) {
                    // functionalities to show/hide the iframe

                    $rootScope.$on('hideIframe', function () {
                        $scope.$apply(function () {
                            $scope.showIframe = false;
                        });
                    });

                    $rootScope.$on('showIframe', function () {
                        $scope.$apply(function () {
                            $scope.showIframe = true;
                        });
                    });
                }

            },
            templateUrl: PATH_CONFIG.BRAVOURE_COMPONENTS + 'angular-block-signup-form-iframe/block-signup-form-iframe.html'
        }
    }

    blockSignupFormIframe.$inject = ['PATH_CONFIG', '$rootScope'];

    angular
      .module('bravoureAngularApp')
      .directive('blockSignupFormIframe', blockSignupFormIframe);

})();
